import '../styles/index.scss';
let photoCollection;

$(document).ready(function () {
    $.ajax({
        url: 'https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=People&per_page=15&page=1',
        type: 'GET',
        headers: {
            "Authorization": `Bearer 563492ad6f91700001000001a02b7e302d6a4f6287f59c1704e1e289`
        }
    }).done(data => {
        console.log(data);
        photoCollection = data.photos;
        renderPhotos(photoCollection);
    });
});

function renderPhotos(photos) {
    $.each(photos, (i, photo) => {
        $('.row').append($(createCard(photo)));
    });
}

function filterByAuthor(photos, author) {
   const filteredPhotos =  photos.filter(photo => photo.photographer === author);
   return filteredPhotos;
}

function createCard(photo) {
    return `<div class="col-md-3">
    <div class="card h-100">
        <div class="card-img">
            <img class="card-img-top" src="${photo.src.medium}"/>
        </div>
        <div class="card-body">
            <h5>${photo.photographer}</h5>
        </div>
    </div>
</div>`;
}

const searchButton = document.getElementById('button-addon5');
const searchInput = document.getElementById("search-input");

searchButton.onclick = () =>{
    const filteredPictures = filterByAuthor(photoCollection, searchInput.value);

    if(filteredPictures.length === 0){
        console.log("No such author found!");
        searchInput.value = "";
        searchInput.placeholder = "No such author found!";
    } else {
        $('.row').children().remove();
        renderPhotos(filteredPictures);
    }
}

const resetButton = document.getElementById('reset-button');

resetButton.onclick = () => {
    $('.row').children().remove();
    renderPhotos(photoCollection);
}
